#! /bin/bash

set -e

DOTFILES=$(dirname $0)
SKIP_FILES='skip_files'
SKIP_DIRS='skip_dirs'

cd $DOTFILES

echo "> symlinking regular files"
for i in $(find . -maxdepth 1 -type f -name ".*" | sort); do
    base=$(basename $i)
    if [[ -n "$(\egrep -o $base$ $SKIP_FILES)" ]]; then
        echo "skipping $base"
        continue
    fi
    printf "checking $base... "
    s="$(pwd)/$base"
    t="$HOME/$base"
    if [[ -h "$t" ]]; then
        echo "$t is already linked, skipping"
        continue
    fi
    if [[ -f "$t" ]]; then
        echo "$t is a regular file, it may differ from that of dotfiles, just showing diff"
        set +e; diff --color=auto $s $t; set -e
        continue
    fi
    printf "not found, creating symlink: "
    echo "ln -s $s $t"
    ln -s $s $t
done

echo "> symlinking directories"
for i in $(find . -maxdepth 1 -type d -regex './\..*'| sort); do
    base=$(basename $i)
    if [[ -n "$(\grep -o $base$ $SKIP_DIRS)" ]]; then
        echo "skipping $base"
        continue
    fi
    printf "checking dir $base... "
    s="$(pwd)/$base"
    t="$HOME/$base"
    if [[ -h "$t" ]]; then
        echo "$t is already linked, skipping"
        continue
    fi
    if [[ -d "$t" ]]; then
        echo "$t is a directory, it may differ from that of dotfiles, please check manually, skipping"
        continue
    fi
    printf "not found, creating symlink: "
    echo "ln -s $s $t"
    ln -s $s $t
done
