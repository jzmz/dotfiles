# josh-sorin.zsh-theme
# for color palette, use spectrum_ls function

function vi_mode_prompt_info() {
  test -z "$KEYMAP" && echo "${RESET}[$INS_INDICATOR${RESET}]" && return 0
  echo "${RESET}[${${KEYMAP/vicmd/$CMD_INDICATOR}/(main|viins)/$INS_INDICATOR}${RESET}]"
}

# TODO Move this function to a corresponding plugin dir
function tf_env() {
  local env=""
  local env_path="$PWD/.terraform/environment"
  if [ -f "$env_path" ]; then
    env=$(<$env_path)
  fi
  if [ -n "$env" ]; then
    env=" ${ZSH_THEME_TF_PROMPT_PREFIX}$env${ZSH_THEME_TF_PROMPT_SUFFIX}"
  fi
  echo "$env"
}

if [[ "$TERM" != "dumb" ]] && [[ "$DISABLE_LS_COLORS" != "true" ]]; then
  RESET="%{$reset_color%}"
  CMD_INDICATOR="%{$fg_bold[red]%}n${RESET}"
  INS_INDICATOR="%{$fg_bold[blue]%}i${RESET}"

  local prompt_with_return_status="%{$fg_bold[red]%}%(?..>>)%{$fg_bold[blue]%}%(?.>>.)${RESET}"
  local where_am_i="%{$fg_bold[green]%}%c${RESET}"
  
  PROMPT='${where_am_i} $(vi_mode_prompt_info)$(git_prompt_info)$(git_prompt_status)$(tf_env)
${prompt_with_return_status} '

  ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[blue]%}(git:${RESET}%{$fg_bold[red]%}"
  ZSH_THEME_GIT_PROMPT_SUFFIX="${RESET}%{$fg[blue]%})${RESET}"
  ZSH_THEME_GIT_PROMPT_DIRTY="${RESET}%{$fg[black]%}*${RESET}"
  ZSH_THEME_GIT_PROMPT_CLEAN=""

  RPROMPT=

  ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%} +"
  ZSH_THEME_GIT_PROMPT_MODIFIED="%{$FG[003]%} m"
  ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%} -"
  ZSH_THEME_GIT_PROMPT_RENAMED="%{$FG[006]%} r"
  ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[yellow]%} ═"
  ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$FG[013]%} u"

  ZSH_THEME_TF_PROMPT_PREFIX=" %{$fg[magenta]%}(tf:${RESET}%{$fg_bold[magenta]%}"
  ZSH_THEME_TF_PROMPT_SUFFIX="${RESET}%{$fg[magenta]%})${RESET}"
else 
  MODE_INDICATOR=" N"
  local return_status="%(?::⏎)"
  
  PROMPT='%c$(git_prompt_info) %(!.#.>) '

  ZSH_THEME_GIT_PROMPT_PREFIX=" ("
  ZSH_THEME_GIT_PROMPT_SUFFIX=")"
  ZSH_THEME_GIT_PROMPT_DIRTY=""
  ZSH_THEME_GIT_PROMPT_CLEAN=""

  #RPROMPT='${return_status}$(git_prompt_status)$(vi_mode_prompt_info)'

  ZSH_THEME_GIT_PROMPT_ADDED=" ✚"
  ZSH_THEME_GIT_PROMPT_MODIFIED=" ✹"
  ZSH_THEME_GIT_PROMPT_DELETED=" ✖"
  ZSH_THEME_GIT_PROMPT_RENAMED=" ➜"
  ZSH_THEME_GIT_PROMPT_UNMERGED=" ═"
  ZSH_THEME_GIT_PROMPT_UNTRACKED=" ✭"
fi
