#!/bin/bash

cd $(dirname $0)/..

apt_packages=(git zsh tmux xclip vim-gtk3 python3-pip jq)
pip3_packages=(awscli pgcli psycopg2-binary mycli docker-compose tmuxp)

echo ">> Execute link.sh"
bash link.sh

echo ">> Install base APT packages (${apt_packages[@]})"
sudo apt install -y ${apt_packages[@]}

echo ">> Install base Python3 (PIP) packages (${pip3_packages[@]})"
pip3 install --user --upgrade ${pip3_packages[@]}
